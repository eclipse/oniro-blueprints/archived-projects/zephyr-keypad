.. _zephyr-keypad:

Keypad blueprint for Zephyr based on LVGL
#########################################

Building
********

.. code-block:: shell

    cd zephyr-keypad
    ZEPHYR_BASE=<path to zephyr> west build -b <board> -p auto ./
