// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-License-Identifier: MIT

#include <device.h>
#include <drivers/display.h>
#include <lvgl.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(app);

LV_IMG_DECLARE(icon1);
LV_IMG_DECLARE(icon2);
LV_IMG_DECLARE(icon3);
LV_IMG_DECLARE(icon4);
LV_IMG_DECLARE(icon5);
LV_IMG_DECLARE(icon6);

static const lv_img_dsc_t *icons_row1[] = {
	&icon1, &icon2, &icon3
};

static const lv_img_dsc_t *icons_row2[] = {
	&icon4, &icon5, &icon6
};

static const lv_img_dsc_t **icons[] = {
	icons_row1, icons_row2
};

static lv_obj_t *make_main_container(void)
{
	lv_obj_t *cont;

	cont = lv_cont_create(lv_scr_act(), NULL);
	lv_cont_set_layout(cont, LV_LAYOUT_COLUMN_MID);
	lv_cont_set_fit(cont, LV_FIT_PARENT);
	lv_obj_align(cont, NULL, LV_ALIGN_CENTER, 0, 0);

	cont = lv_cont_create(cont, NULL);
	lv_cont_set_layout(cont, LV_LAYOUT_COLUMN_MID);
	lv_cont_set_fit(cont, LV_FIT_TIGHT);
	lv_obj_align(cont, NULL, LV_ALIGN_CENTER, 0, 0);

	return cont;
}

static lv_obj_t *make_row(lv_obj_t *cont)
{
	lv_obj_t *row;

	row = lv_cont_create(cont, NULL);
	lv_obj_align(row, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
	lv_cont_set_layout(row, LV_LAYOUT_ROW_MID);
	lv_cont_set_fit(row, LV_FIT_TIGHT);

	return row;
}

static void init_button_style(lv_style_t *style)
{
	lv_style_init(style);
	lv_style_set_image_recolor_opa(style, LV_STATE_PRESSED, LV_OPA_30);
	lv_style_set_image_recolor(style, LV_STATE_PRESSED, LV_COLOR_WHITE);
}

static void init_row_style(lv_style_t *style)
{
	lv_style_init(style);
	lv_style_set_border_width(style, LV_STATE_DEFAULT, 0);
}

static void button_event_handler(lv_obj_t *obj, lv_event_t event)
{
	if (event == LV_EVENT_CLICKED)
		printk("Button clicked\n");
}

static struct bt_uuid_128 keypad_uuid = BT_UUID_INIT_128(
        0x03, 0x00, 0x13, 0xac, 0x42, 0x02, 0xcd, 0x8d,
        0xeb, 0x11, 0xbb, 0x88, 0x08, 0x54, 0xd1, 0x32);

static struct bt_uuid_128 keypress_uuid = BT_UUID_INIT_128(
	0x03, 0x00, 0x13, 0xac, 0x42, 0x02, 0xcd, 0x8d,
	0xeb, 0x11, 0xbb, 0x88, 0x09, 0x54, 0xd1, 0x32);

BT_GATT_SERVICE_DEFINE(keypad_service,
	BT_GATT_PRIMARY_SERVICE(&keypad_uuid),
	BT_GATT_CHARACTERISTIC(&keypress_uuid.uuid,
		BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_NONE, NULL, NULL, NULL));

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA_BYTES(BT_DATA_UUID16_ALL,
	BT_UUID_16_ENCODE(BT_UUID_HRS_VAL)),
};

void main(void)
{
	const lv_img_dsc_t *icon;
	const struct device *display;
	lv_obj_t *cont, *row[2], *btn;
	lv_style_t btn_style, row_style;
	int ret, i, j;

	display = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);
	if (!display) {
		LOG_ERR("Display not found, aborting.");
		return;
	}

	ret = bt_enable(NULL);
	if (ret) {
		LOG_ERR("Unable to initialize bluetooth: %d", ret);
		return;
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (ret) {
		LOG_ERR("Unable to advertise bluetooth services: %d", ret);
		return;
	}

	init_button_style(&btn_style);
	init_row_style(&row_style);

	cont = make_main_container();
	for (i = 0; i < 2; i++) {
		row[i] = make_row(cont);
		lv_obj_add_style(row[i], LV_CONT_PART_MAIN, &row_style);
	}


	for (i = 0; i < 2; i++) {
		for (j = 0; j < 3; j++) {
			icon = icons[i][j];

			btn = lv_imgbtn_create(row[i], NULL);
			lv_imgbtn_set_src(btn, LV_BTN_STATE_RELEASED, icon);
			lv_imgbtn_set_src(btn, LV_BTN_STATE_PRESSED, icon);
			lv_obj_set_style_local_bg_opa(btn, LV_OBJ_PART_MAIN,
						      LV_STATE_DEFAULT,
						      LV_OPA_TRANSP);
			lv_obj_set_event_cb(btn, button_event_handler);
			lv_obj_add_style(btn, LV_IMGBTN_PART_MAIN, &btn_style);
		}
	}

	lv_task_handler();
	display_blanking_off(display);

	for (;;) {
		lv_task_handler();
		k_sleep(K_MSEC(10));
	}
}
